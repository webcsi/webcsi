/* this are some example snippets
for execution (replacing below), select snippet and press the run button (ctrl+space)
for pinning execution (adding above), select snippet, check(ctrl+y) pining and press the run button */

///// https://sv443.net/jokeapi/v2/?ref=apilist.fun
const {Rest} = await wt.import("modules", "session/rest");
const joke_session = Rest.session(wt, "https://v2.jokeapi.dev/joke");
assert_err(res = await joke_session.get(["Any"], {lang: "en", blacklistFlags: "racist,sexist"}));
console.log(res);
ctl.success();
////////////////////////////////////////////////////////////////////////////////

///// https://reqres.in/
const {Rest} = await wt.import("modules", "session/rest")
const dummy_session = Rest.session(wt, "https://reqres.in/api");
assert_err(res = await dummy_session.get(["users", 2]));
console.log(res);
ctl.success();
////////////////////////////////////////////////////////////////////////////////

///// https://reqres.in/
const {Rest} = await wt.import("modules", "session/rest")
const dummy_session = Rest.session(wt, "https://reqres.in/api");
assert_err(res = await dummy_session.post(["users"], {name: "morpheus", job: "leader"}));
console.log(res);
ctl.success();
////////////////////////////////////////////////////////////////////////////////

///// https://reqres.in/
const {Rest} = await wt.import("modules", "session/rest")
const dummy_session = Rest.session(wt, "https://reqres.in/api");
assert_err(res = await dummy_session.put(["users", 2], {name: "morpheus", job: "zion resident"}));
console.log(res);
ctl.success();
////////////////////////////////////////////////////////////////////////////////

///// https://reqres.in/
const {Rest} = await wt.import("modules", "session/rest")
const dummy_session = Rest.session(wt, "https://reqres.in/api");
assert_err(res = await dummy_session.patch(["users", 2], {name: "morpheus", job: "zion resident"}));
console.log(res);
ctl.success();
////////////////////////////////////////////////////////////////////////////////

///// https://reqres.in/
const {Rest} = await wt.import("modules", "session/rest")
const dummy_session = Rest.session(wt, "https://reqres.in/api");
assert_err(res = await dummy_session.delete(["users", 2]));
console.log(res);
ctl.success();
////////////////////////////////////////////////////////////////////////////////

///// https://reqres.in/ + vis-network
const {Rest} = await wt.import("modules", "session/rest")
const {DataSet, Network} = await import("vis-network");

const dummy_session = Rest.session(wt, "https://reqres.in/api");
assert_err(res = await dummy_session.get(["users"], {page: 2}));

const data = {
    nodes: new DataSet(),
    edges: new DataSet()
};

for(const u of res.data)
    data.nodes.add({id: u.id, label: u.first_name+" "+u.last_name});

$("#frame").append(`<div class="bg-light net" style="height: 500px"></div>`);
let hosts_vis = new Network($("#frame .net")[0], data, {});
hosts_vis.on('afterDrawing', () => ctl.success());
////////////////////////////////////////////////////////////////////////////////