export class JsonRpcSession {
    constructor(wt, url, options) {
        this.running = 0;
        this.wt = wt;
        this.url = url;
        this.options = options;

        this._validate();

        this.loader = this._load();
    }

    async _load() {
        this.axios = await this.wt.import("imports", "axios");
    }

    _validate() {
        if(this.url !== undefined) {
            if(!this.url.search("^(http(s):\/\/.)[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)$")) {
                throw("JsonRPC: Invalid url format");
            }

            if(this.options === undefined)
                this.options = {};

            if(this.options.cache === undefined)
                this.options.cache = false;

            if(this.options.headers === undefined)
            this.options.headers = {};
        }
    }

    // Request method must be a string
    _validateRequestMethod(method) {
        if(typeof(method) !== 'string')
            throw("JsonRPC: Invalid method supplied for JsonRPC request");
    }

    // Determines the appropriate request URL to call for a request
    _requestUrl() {
        const ts = new Date().getTime();
        return this.url.includes("?") ? this.url+"timestamp="+ts : this.url+"?timestamp="+ts;
    }

    async request(method, params, auth = undefined, opts = {}) {
        const func_ctx = "JsonRPC::request";
        await this.loader;
        if(this.url !== undefined) {
            this._validateRequestMethod(method);
            
            const data = {
                "jsonrpc": "2.0",
                "method": method,
                "id": ++this.running,
                "params": params
            };
            if(opts.headers === undefined)
                opts.headers = {};
            opts.headers['Content-Type'] = 'application/json';
            opts.headers['Cache-Control'] = 'no-cache';
            opts.headers['Pragma'] = 'no-cache';
            opts.headers['Expires'] = '0';
            if(auth !== undefined)
                data.auth = auth;
            
            try {
                const res = await this.axios.default.post(this._requestUrl(), data, opts);
                return res.data;
            } catch(e) {
                return {
                    "error": e,
                    "context": func_ctx,
                    "method": method,
                    //"params": params
                };
            }
        } else {
            console.log("Mock "+func_ctx, method);
            return params;
        }
    }
}

export class JsonRpc {
    static async session(wt, url) {
        let s = new JsonRpcSession(wt, url);
        await s.loader;
        return s;
    }
}