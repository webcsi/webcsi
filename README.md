## Name
Web Command Script Interface

## Description
A HTML/JS app for executing javascript snippets against REST, OpenAPI, JsonRPC, ... interfaces and visualizing results utilizing visualization javascript libraries like vis.js, D3js, ...

## Installation
Just place it on a web server. There is no server side scripting or whatsoever required.

## Authors and acknowledgment
### Creators
- Ralph Alexander Bariz

### Contributors

## License
GNU Affero General Public License v3
