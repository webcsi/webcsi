export class RestSession {
    constructor(wt, url, options) {
        this.running = 0;
        this.wt = wt;
        this.url = url;
        this.options = options;

        this._validate();

        this.loader = this._load();
    }

    async _load() {
        this.axios = await this.wt.import("imports", "axios");
    }

    _validate() {
        if(this.url !== undefined) {
            if(!this.url.search("^(http(s):\/\/.)[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)$")) {
                throw("REST: Invalid url format");
            }

            if(this.options === undefined)
                this.options = {};

            if(this.options.headers === undefined)
                this.options.headers = {};
            this.options.headers['Content-Type'] = 'application/json';
        }
    }

    // Request path must be an array of string
    _validateRequestPath(path) {
        if(!Array.isArray(path))
            throw("REST: Invalid path supplied for OpanAPI get");
    }

    // Determines the appropriate get URL to call for a get
    _getUrl(path, params) {
        let url = this.url;
        for(const p of path)
            url += "/"+p;

        if(params !== undefined) {
            url += "?"
            for(const p in params) 
                url += p+"="+params[p]+"&"
        }

        const ts = new Date().getTime();
        return url.includes("?") ? url+"timestamp="+ts : url+"?timestamp="+ts;
    }

    _process_params(func_ctx, params) {
        if(params === undefined || params == null)
            params = {};

        if(typeof params !== 'object')
            return {
                "error": "params needs to be an object containing key => value string pairs",
                "context": func_ctx};

        return params;
    }

    async get(path, params) {
        const func_ctx = "REST::get";
        await this.loader;
        if(this.url !== undefined) {
            this._validateRequestPath(path);
            
            try {
                const res = await this.axios.default.get(this._getUrl(path, params), this.options);
                return res.data;
            } catch(e) {
                return {
                    "error": e,
                    "context": func_ctx,
                    "path": path,
                    //"params": params
                };
            }
        } else {
            console.log("Mock "+func_ctx, path);
            return params;
        }
    }

    async post(path, params) {
        const func_ctx = "REST::post";
        await this.loader;
        if(this.url !== undefined) {
            this._validateRequestPath(path);
            
            try {
                const res = await this.axios.default.post(this._getUrl(path), params, this.options);
                return res.data;
            } catch(e) {
                return {
                    "error": e,
                    "context": func_ctx,
                    "path": path,
                    //"params": params
                };
            }
        } else {
            console.log("Mock "+func_ctx, path);
            return params;
        }
    }

    async put(path, params) {
        const func_ctx = "REST::put";
        await this.loader;
        if(this.url !== undefined) {
            this._validateRequestPath(path);
            
            try {
                const res = await this.axios.default.put(this._getUrl(path), params, this.options);
                return res.data;
            } catch(e) {
                return {
                    "error": e,
                    "context": func_ctx,
                    "path": path,
                    //"params": params
                };
            }
        } else {
            console.log("Mock "+func_ctx, path);
            return params;
        }
    }

    async patch(path, params) {
        const func_ctx = "REST::patch";
        await this.loader;
        if(this.url !== undefined) {
            this._validateRequestPath(path);
            
            try {
                const res = await this.axios.default.patch(this._getUrl(path), params, this.options);
                return res.data;
            } catch(e) {
                return {
                    "error": e,
                    "context": func_ctx,
                    "path": path,
                    //"params": params
                };
            }
        } else {
            console.log("Mock "+func_ctx, path);
            return params;
        }
    }

    async delete(path, params) {
        const func_ctx = "REST::delete";
        await this.loader;
        if(this.url !== undefined) {
            this._validateRequestPath(path);
            
            try {
                const res = await this.axios.default.delete(this._getUrl(path, params), params, this.options);
                return res.status == 204;
            } catch(e) {
                return {
                    "error": e,
                    "context": func_ctx,
                    "path": path,
                    //"params": params
                };
            }
        } else {
            console.log("Mock "+func_ctx, path);
            return params;
        }
    }
}

export class Rest {
    static async session(wt, url, options) {
        let s = new RestSession(wt, url, options);
        await s.loader;
        return s;
    }
}