///// https://reqres.in/
const {Rest} = await import("session/rest.js")
const dummy_session = Rest.session("https://reqres.in/api");
assert_err(res = await dummy_session.patch(["users", 2], {name: "morpheus", job: "zion resident"}));
console.log(res);
ctl.success();
////////////////////////////////////////////////////////////////////////////////