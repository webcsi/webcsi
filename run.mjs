'use strict';

const console_log = console.log.bind(console);
const console_warn = console.warn.bind(console);
const console_error = console.error.bind(console);

console.log = (...params) => {
    let text = params.map(param =>
        typeof param === 'object'
        ? JSON.stringify(param, null, 4)
        : param
    );
        
    console_log(...text);
};

console.warn = (...params) => {
    let text = params.map(param =>
        typeof param === 'object'
        ? JSON.stringify(param, null, 4)
        : param
    );

    console_warn(...text);
};

console.error = (...params) => {
    let text = params.map(param =>
        typeof param === 'object'
        ? JSON.stringify(param, null, 4)
        : param
    );

    console_error(...text);
};

if(process.argv.length != 3)
    throw ("no script name given or unknown arguments");

const script_name = process.argv[2];
const fs = await import('fs');
const file_name = "scripts/"+script_name+".js";

class Ctl {
    success() {
        console.log("===success===");
    }

    fail(err) {
        console.error(err);
    }
}

const run_tools = `
function is(value) { return value !== undefined; }
function assert(condition, err) { if(!condition) throw err; }
function assert_err(test) { assert(!is(test.error) || !test.error != false, test); }
`.replace("\n", "");

fs.readFile(file_name, 'utf8', async function(err, data) {
    let ctl = new Ctl();
    let js = 
        `${run_tools} async function wrap(ctl) {try{${data};
        } catch(e){ctl.fail(e)}}
        wrap(c);`;
    let fn = new Function("c", js);
    fn(ctl);
});

